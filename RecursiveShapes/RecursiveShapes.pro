#-------------------------------------------------
#
# Project created by QtCreator 2014-05-24T12:21:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RecursiveShapes
TEMPLATE = app


SOURCES += main.cpp\
    line.cpp \
    drawingWindow.cpp

HEADERS  += \
    line.h \
    drawingWindow.h

FORMS    += \
    drawingWindow.ui
