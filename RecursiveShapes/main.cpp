
#include <QApplication>
#include <iostream>
#include <QLineEdit>
#include <QObject>
#include <QAction>
#include <QPushButton>
#include <cmath>
#include <QDebug>

#include "drawingWindow.h"


// just a square of size *sideLength* because I am too lazy to define a
// square widget.

void box(int x, int y, int sideLength, QColor c, DrawingWindow &w) {
    w.addLine(x,y,x+sideLength,y,1,c);
    w.addLine(x+sideLength,y,x+sideLength,y+sideLength,1,c);
    w.addLine(x+sideLength,y+sideLength,x,y+sideLength,1,c);
    w.addLine(x,y+sideLength,x,y,1,c);
}


//Esta es la funcion que debes completar

void boxes(int x, int y, int sideLength, double shrinkFactor, int smallestLength, QColor c, DrawingWindow &w) {
   
    
    // Tu codigo para boxes
    
    
}

// Here is another recursive figure, the snowflake presented at:
// https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-6-turtle-recursion
// sf = sf(n-1) + 60 grados sf(n-1) + 60 grados sf(n-1) + s(n-1)
// sf(0) = line

void snowflake(int x, int y, int size, double angle, int level, QColor c, DrawingWindow &w) {
    int chunkSize = round(size/3);

    if (level == 0) {
        w.addLinePolar(x,y,size,angle,1,c);
        return;
    }


    snowflake(x, y, chunkSize, angle, level-1, c, w);

    x = x + round( chunkSize * cos(angle * M_PI/180));
    y = y + round( chunkSize * sin(angle * M_PI/180));
    snowflake(x, y, chunkSize, angle + 60, level-1, c, w);

    snowflake(x + round(chunkSize * cos((angle + 60) * M_PI/180)),
                  y + round(chunkSize * sin((angle + 60) * M_PI/180)),
                  chunkSize, angle - 60, level-1, c, w);

    x = x + round(chunkSize * cos(angle * M_PI/180));
    y = y + round(chunkSize * sin(angle * M_PI/180));

    snowflake(x, y, chunkSize, angle, level-1, c, w);
}

// interface function for the snowflake recursive function

void snowHelper(int size, int level, DrawingWindow &w) {
    snowflake(0,size,size,0,level,Qt::blue,w);
    snowflake(round(size*cos(-60*M_PI/180)),size+round(size*sin(-60*M_PI/180)),size,120,level,Qt::blue,w);
    snowflake(size,size,size,-120,level,Qt::blue,w);
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;

    w.resize(500, 500);
    w.show();


    int size = 333, level = 3;
    snowHelper(size,level,w);

    // Invocacion a  funcion boxes

    return a.exec();

}


