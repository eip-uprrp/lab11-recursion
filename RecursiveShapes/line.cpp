#include "line.h"
#include <QDebug>
#include <cmath>

using namespace std;
Line::Line(QWidget *parent) :
    QWidget(parent)
{
    x0 = y0 = x1 = y1 = 0;
    penColor = Qt::black;
    penWidth = 1;
}

///
/// \brief Line::Line - Constructor for a line, specifying the (fromX,fromY) and (toX,toY)
/// \param fromX: starting x coordinate
/// \param fromY: starting y coordinate
/// \param toX: end x coordinate
/// \param toY: end y coordinate
/// \param w: pen width
/// \param c: line color
/// \param parent: parent of this line
///

Line::Line(int fromX, int fromY, int toX, int toY, int w, QColor c, QWidget *parent):
    QWidget(parent), x0(fromX), y0(fromY), x1(toX), y1(toY), penWidth(w),  penColor(c)
{
    resize(max(x0,x1)+1,max(y0,y1)+1); 
}

///
/// \brief Line::Line - Constructore for a line, specifying (fromX, fromY) and the **length** and **angle**.
/// \param fromX: starting x coordinate
/// \param fromY: starting y coordinate
/// \param length: length of the line
/// \param angle: angle
/// \param w: pen width
/// \param c: line color
/// \param parent: parent of this line
///
Line::Line(int fromX, int fromY, int length, double angle, int w, QColor c, QWidget *parent):
    QWidget(parent), x0(fromX), y0(fromY),  penWidth(w), penColor(c)
{
    double angleRad = M_PI * angle / 180.;
    x1 = x0 + round(length * cos(angleRad));
    y1 = y0 + round(length * sin(angleRad));
    resize(max(x0,x1)+1,max(y0,y1)+1);
}

///
/// \brief Line::setCoords - setter for the line coordinates
/// \param fromX: starting x coordinate
/// \param fromY: starting y coordinate
/// \param toX: end x coordinate
/// \param toY: end y coordinate
///
void Line::setCoords(int fromX, int fromY, int toX, int toY) {
    x0 = fromX;
    y0 = fromY;
    x1 = toX;
    y1 = toY;
    resize(max(x0,x1)+1,max(y0,y1)+1);
}

///
/// \brief Line::setpenColor - setter for the pen color
/// \param c - line color
///

void Line::setpenColor(QColor c) { penColor = c; }

///
/// \brief Line::setPenWidth - setter for the pen width
/// \param w - pen width
///

void Line::setPenWidth(int w)    { penWidth = w; }

int Line::getX0() { return x0; }
int Line::getY0() { return y0; }
int Line::getX1() { return x1; }
int Line::getY1() { return y1; }

///
/// \brief Line::paintEvent - The paint event function is automatically invoked
/// whenever a resize or repaint happens.
/// \param event - contains info about the event that caused the repaint
///

void Line::paintEvent(QPaintEvent *event) {
    QPainter p(this);
    QPen myPen;
    myPen.setWidth(penWidth);
    myPen.setColor(penColor);

    p.setPen(myPen);
    p.drawLine(x0,y0,x1,y1);
}
