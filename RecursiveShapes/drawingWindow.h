#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include "line.h"


using namespace std;
namespace Ui {
class DrawingWindow;
};

class DrawingWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DrawingWindow(QWidget *parent = 0);
    ~DrawingWindow();
    void addLine(int x0, int y0, int x1, int y1, int width, QColor color);
    void addLinePolar(int x0, int y0, int length, double angle, int width, QColor color);
private:
    Ui::DrawingWindow *ui;
    vector <Line *> *vL;

protected:
    void paintEvent(QPaintEvent *);
};

#endif // MAINWINDOW_H
