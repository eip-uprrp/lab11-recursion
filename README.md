#Lab. 11: Recursión


![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/QoGxuiT.jpg)
![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/0pvQnoX.jpg)
![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/uasFMfZ.png)

**Figura 1.** Mandelbrot set [2], Seehorse tail [3], Fractal [4]

Una técnica utilizada en programación es la *recursión*. Éste es un proceso en el que se definen objetos utilizando objetos similares. Podemos construir conjuntos de objetos o procesos utilizando *reglas recursivas* y *valores iniciales*. Las *funciones recursivas* son funciones que se auto-invocan, utilizando cada vez conjuntos o elementos más pequeños,  hasta llegar a un punto en donde se utiliza el valor inicial en lugar de auto-invocarse. Los fractales son un ejemplo de figuras que se pueden crear usando recursión.


##Objetivos:

En esta experiencia de laboratorio los estudiantes practicarán la definición e implementación de funciones recursivas para dibujar formas auto-similares (fractales).

Los ejercicios de esta experiencia de laboratorio son una adaptación de https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-6-turtle-recursion.

##Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos relacionados a funciones recursivas.

2. haber estudiado la función `box` para dibujar cajas, incluida en el archivo `main.cpp` del proyecto de `Qt`.

3. haber tomado el [quiz Pre-Lab 11](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6885) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


## Figuras auto-similares
![](http://demo05.cloudimage.io/s/resize/400/i.imgur.com/4PTlu0l.jpg)

**Figura 2.** Fractal tree [5]

Una manera ingeniosa de practicar y "visualizar" recursión es programando funciones que produzcan figuras auto-similares (o recursivas). Por ejemplo, considera la siguiente figura recursiva que llamaremos *rama*. La Figura 3 muestra `rama(0,90), rama(1,90)` y `rama(2,90)`.


![](http://i.imgur.com/D99udk8.jpg)

**Figura 3.** (a) `rama(0,90)`, (b) `rama(1,90)`, y (c) `rama(2,90)`. 



?Puedes ver el comportamiento recursivo de esta figura? Nota que `rama(0,90)` es solo un segmento vertical (un segmento en un ángulo de 90 grados); `rama(1,90)` es `rama(0,90)` con dos segmentos inclinados en  su extremo superior. Más preciso, `rama(1,90)` es `rama(0,90)` con una  `rama(0,60)` y una `rama(0,120)` en el extremo superior. Similarmente, `rama(2,90)` es `rama(0,90)`  con dos `rama(1)` inclinadas en el extremo superior. Esto es,  `rama(2,90)` es 
`rama(0,90)` con una `rama(1,60)` y una `rama(1,90)` en el extremo superior.

De esta manera podemos expresar `rama(n,A)` como una composición de ramas con $n$'s más pequeñas inclinadas. Lo siguiente es una manera de expresar `rama` como una función recursiva:

```
rama(0, A) = dibuja un segmento de largo L en un ángulo A
rama(n, A) = dibuja: rama(0,A), rama(n-1, A-30), rama(n-1, A+30)
```

**Código 1: Función para dibujar las ramas**


Nota que nuestra definición recursiva incluye un caso base, esto es, incluye `rama(0,A)`, y una relación de recurrencia (o caso recursivo), esto es, `rama(n,A)`. Para simplificar, asumimos que  `rama(n-1,A-30)` y `rama(n-1,A+30)` se dibujan en el extremo superior de `rama(0,A)`.

La Figura 4 ilustra la expansión recursiva para `rama(2,90)`. El color de cada expresión es el color del segmento que le corresponde en la figura.


![](http://i.imgur.com/voJ0xpN.jpg)

**Figura 4.** Ilustración de `rama(2,90)`.
 
?Puedes predecir cómo será la próxima iteración de la figura? Es decir, ?qué figura producirá `rama(3,A)`?

&nbsp;

## Sesión de laboratorio

En la experiencia de laboratorio de hoy experimentarás el uso de funciones recursivas para producir fractales.

###**Instrucciones:**

1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab11-Recursion.git` para descargar la carpeta `Lab11-Recursion` a tu computadora.

2.  Marca doble "click" en el archivo `RecursiveShapes.pro` para cargar este proyecto a Qt. 



### **Ejercicio 1: Copo de nieve**



Una de las figuras fractales más simples es la figura de un copo de nieve. Esta figura se forma a partir de un triángulo isóseles, sustituyendo el segmento del tercio del medio de cada lado por una "V" invertida. La medida de los lados de la "V" es igual a la medida del segmento que sustituye. Usaremos el copo de nieve para ilustrar el proceso de recursión.

![](http://i.imgur.com/iJAoOlO.png)

**Figura 5.** Parte de la construcción del fractal "copo de nieve".



Corre  `RecursiveShapes` para que veas una figura del copo de nieve construida con 3 iteraciones de la función `snowflake`. Puedes ver esta función en el archivo `main.cpp` del proyecto de `Qt`.

En la función `main`, busca la línea en donde se declara y dá valor a la variable `level`. Cambia el valor de `level` a `0` y corre el programa de nuevo. Podrás ver el triángulo que representa el caso base de la recursión para el copo de nieve. Continúa cambiando el valor y corriendo el programa para que veas el proceso de la recursión y de producir figuras auto-similares.

### **Ejercicio 2: Cajas autosimilares**

Tu tarea en este ejercicio es programar una función recursiva `boxes` que produzca las siguientes figuras. 


![](http://i.imgur.com/fFnMuTX.jpg)

**Figura 6.** Ilustración de las figuras de cajas que debe producir tu programa.

La función recursiva `boxes` incluye tres parámetros: `sideLength`, `shrinkFactor`, `smallestLength`.

* `sideLength`: un entero que determina el largo de los lados del cuadrado más grande.
* `shrinkFactor`: un número real que determina la razón del siguiente nivel de cuadrados. Por ejemplo, si `sideLength` es `100`, y `shrinkFactor` es `0.3`, el largo de los lados del cuadrado más grande será `100` unidades, y el largo de los lados del próximo cuadrado más pequeño  será `100*.3=30` unidades. Se colocan 4 copias de ese cuadrado más pequeño dentro del cuadrado anterior, un cuadrado en cada esquina.
* `smallestLength`: es un valor entero que determina el largo del lado del cuadrado más pequeño que será dibujado. Por ejemplo, en la Figura 6, `boxes(400, 0.4, 200)` solo dibuja el cuadrado con lados de largo `400`, ya que el tamaño que le seguiría sería `400 * 0.4 = 160`, que es más pequeño que `200`. Por otro lado, `boxes(400, 0.4, 75)` dibuja el cuadrado de tamaño `400` y los cuadrados de tamaño `160`, pero no los siguientes en tamaño, porque serían de tamaño `160 * 0.4 = 64`, que es menor que `75`.

**Instrucciones:**

1. Escribe un algoritmo recursivo para la función `boxes`. Recuerda incluir el caso base!. 
2. Implementa la función en `Qt` usando la función `box` que dibuja cajas y que está incluida en `main.cpp`. Necesitarás proveer parámetros adicionales a tu función para que puedas controlar cosas como la posición y el color de los cuadrados. Compara tus resultados con las imágenes de la Figura 6.

**Entrega 1.**

Copia el algoritmo para la función `boxes` en la sección correspondiente de la página de [Entrega 1 del Lab 11](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6886).

**Entrega 2.**

Copia la función `boxes` en la sección correspondiente de la página de [Entrega 2 del Lab 11](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6887). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.


&nbsp;


## Referencias

[1] https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-6-turtle-recursion.

[2] "Mandel zoom 00 mandelbrot set". Licensed under Creative Commons Attribution-Share Alike 3.0 via Wikimedia Commons - http://commons.wikimedia.org/wiki/File:Mandel_zoom_00_mandelbrot_set.jpg#mediaviewer/File:Mandel_zoom_00_mandelbrot_set.jpg

[3] "Mandel zoom 04 seehorse tail". Licensed under Creative Commons Attribution-Share Alike 3.0 via Wikimedia Commons - http://commons.wikimedia.org/wiki/File:Mandel_zoom_04_seehorse_tail.jpg#mediaviewer/File:Mandel_zoom_04_seehorse_tail.jpg

[4] http://www.coolmath.com/fractals/images/fractal5.gif

[5] "Fractal tree (Plate b - 2)". Licensed under Public domain via Wikimedia Commons - http://commons.wikimedia.org/wiki/File:Fractal_tree_(Plate_b_-_2).jpg#mediaviewer/File:Fractal_tree_(Plate_b_-_2).jpg
